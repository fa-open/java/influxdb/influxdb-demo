package com.faber.influxdb;


import com.faber.influxdb.utils.InfluxDBConnection;
import org.influxdb.dto.QueryResult;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TestQuery {

    @Test
    public void testQuery() {
        InfluxDBConnection influxDBConnection = new InfluxDBConnection("root", "root", "http://8.142.130.28:8086", "young_iot_db", "hour");
        QueryResult results = influxDBConnection
                .query("SELECT * FROM us_1 order by time desc limit 1000");
        //results.getResults()是同时查询多条SQL语句的返回值，此处我们只有一条SQL，所以只取第一个结果集即可。
        QueryResult.Result oneResult = results.getResults().get(0);
        if (oneResult.getSeries() != null) {
            List<List<Object>> valueList = oneResult.getSeries().stream().map(QueryResult.Series::getValues)
                    .collect(Collectors.toList()).get(0);
            if (valueList != null && valueList.size() > 0) {
                for (List<Object> value : valueList) {
                    Map<String, String> map = new HashMap<String, String>();
                    // 数据库中字段1取值
                    String field1 = value.get(0) == null ? null : value.get(0).toString();
                    // 数据库中字段2取值
                    String field2 = value.get(1) == null ? null : value.get(1).toString();
                }
            }
        }
    }

}
